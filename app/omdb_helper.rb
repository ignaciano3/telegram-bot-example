require_relative '../exceptions/movie_exceptions'

class OmdbHelper
  @url = 'http://www.omdbapi.com/'
  @api_key = ENV['OMDB_API_KEY']

  def self.get_movie_awards(movie_name)
    response = Faraday.get(@url, { t: movie_name, apikey: @api_key })
    json = validate_response(response)
    json['Awards']
  rescue NotFoundMovieException
    "#{movie_name} not found!"
  rescue MovieException
    'Error'
  end

  private_class_method def self.validate_response(response)
    json = JSON.parse(response.body)
    if json.include?('Error')
      case json['Error']
      when 'Movie not found!'
        raise NotFoundMovieException
      else
        raise MovieException
      end
    end
    json
  end
end
