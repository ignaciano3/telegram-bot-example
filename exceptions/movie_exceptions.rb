class MovieException < StandardError
end

class NotFoundMovieException < MovieException
  def initialize(msg = 'Movie not found')
    super
  end
end
